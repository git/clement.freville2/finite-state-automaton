/**
 * @typedef State
 * @property {Object.<string, number[]>} transitions
 * @property {boolean} [start]
 * @property {boolean} [accepting]
 */

/**
 * @type {State[]}
 */
export const STARTS_ENDS_A = [
  { transitions: { a: [1], b: [3] } },
  { transitions: { a: [2], b: [1] } },
  { transitions: { a: [2], b: [1] }, accepting: true },
  { transitions: { a: [3], b: [3] } },
];

/**
 * @type {State[]}
 */
export const STARTS_BB = [
  { transitions: { a: [3], b: [1] } },
  { transitions: { a: [3], b: [2] } },
  { transitions: { a: [2], b: [2] }, accepting: true },
  { transitions: { a: [3], b: [3] } },
];

/**
 * @type {State[]}
 */
export const EXACTLY_ONE_B = [
  { transitions: { a: [0], b: [1] } },
  { transitions: { a: [1], b: [2] }, accepting: true },
  { transitions: { a: [2], b: [2] } },
];

/**
 * @type {State[]}
 */
export const ODD_NUMBER_OF_A = [
  { transitions: { a: [1], b: [0] } },
  { transitions: { a: [0], b: [1] }, accepting: true },
];

/**
 * @type {State[]}
 */
export const ENDS_WITH_TWO_B = [
  { transitions: { a: [0], b: [1] } },
  { transitions: { a: [0], b: [2] } },
  { transitions: { a: [0], b: [2] }, accepting: true },
];

export const AUTOMATONS = {
  'Starts and ends with a': STARTS_ENDS_A,
  'Starts with bb': STARTS_BB,
  'Exactly one b': EXACTLY_ONE_B,
  'Odd number of a': ODD_NUMBER_OF_A,
  'Ends with two b': ENDS_WITH_TWO_B,
};
