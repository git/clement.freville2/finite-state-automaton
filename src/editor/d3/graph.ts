import type { SimulationLinkDatum, SimulationNodeDatum } from 'd3';

export class Graph {
  private idCounter = 0;
  public readonly nodes: Node[] = [];
  public readonly links: Link[] = [];

  public createNode(x?: number, y?: number): Node {
    const node = new Node(this.idCounter++, x, y);
    this.nodes.push(node);
    return node;
  }

  public createLink(sourceId: number, targetId: number, label: string = 'a'): Link | null {
    const existingLink = this.links.find(
      (l) => l.source.id === sourceId && l.target.id === targetId,
    );
    if (typeof existingLink !== 'undefined') {
      return null;
    }

    const source = this.nodes.find((node) => node.id === sourceId);
    if (typeof source === 'undefined') {
      return null;
    }

    const target = this.nodes.find((node) => node.id === targetId);
    if (typeof target === 'undefined') {
      return null;
    }

    const link = new Link(source, target, label);
    this.links.push(link);
    return link;
  }

  public removeNode(node: Node): [Node, Link[]] | undefined {
    const nodeIndex = this.nodes.findIndex((n) => n.id === node.id);
    if (nodeIndex === -1) {
      return undefined;
    }

    this.nodes.splice(nodeIndex, 1);
    const attachedLinks = this.links.filter(
      (link) => link.source.id === node.id || link.target.id === node.id,
    );
    attachedLinks.forEach((link) => {
      const linkIndex = this.links.indexOf(link, 0);
      this.links.splice(linkIndex, 1);
    });

    return [node, attachedLinks];
  }

  public removeLink(link: Link): Link | undefined {
    const linkIndex = this.links.findIndex(
      (l) => l.source.id === link.source.id && l.target.id === link.target.id,
    );
    if (linkIndex === -1) {
      return undefined;
    }

    this.links.splice(linkIndex, 1);
    return link;
  }

  forEach(consumer: (node: Node, index: number, array: Node[]) => void): void {
    this.nodes.forEach(consumer);
  }
}

export class Node implements SimulationNodeDatum {
  public constructor(
    public readonly id: number,
    public x?: number,
    public y?: number,
    public fx?: number,
    public fy?: number,
    public start: boolean = false,
    public accepting: boolean = false,
    public active: boolean = false,
  ) {}
}

export class Link implements SimulationLinkDatum<Node> {
  public constructor(
    public readonly source: Node,
    public readonly target: Node,
    public transition: string,
  ) {}
}
