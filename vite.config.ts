import { defineConfig } from 'vite';

export default defineConfig({
  base: '',
  build: {
    target: 'esnext',
    modulePreload: false,
    rollupOptions: {
      external: ['d3'],
      output: {
        paths: {
          d3: 'https://cdn.jsdelivr.net/npm/d3@7/+esm',
        },
      },
    },
  },
});
