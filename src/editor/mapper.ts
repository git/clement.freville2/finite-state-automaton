import { Graph } from './d3/graph.ts';

type State = {
  transitions: Record<string, number[]>;
  start?: boolean;
  accepting?: boolean;
};

export function createGraph(states: State[]): Graph {
  const graph = new Graph();
  for (let i = 0; i < states.length; i++) {
    const node = graph.createNode(i);
    if (states[i].accepting) {
      node.accepting = true;
    }
    if (i === 0) {
      node.start = true;
    }
  }
  for (let i = 0; i < states.length; i++) {
    const state = states[i];
    for (const [letter, targets] of Object.entries(state.transitions)) {
      for (const target of targets) {
        graph.createLink(i, target, letter);
      }
    }
  }
  return graph;
}

export function createStateList(graph: Graph): State[] {
  const states: State[] = [];
  // Map node ids to state ids, as graph nodes may contain gaps.
  const reduced: { [id: number]: number } = {};
  for (const node of graph.nodes) {
    const state: State = {
      transitions: {},
      start: node.start,
      accepting: node.accepting,
    };
    reduced[node.id] = states.length;
    states.push(state);
  }
  for (const link of graph.links) {
    const source = reduced[link.source.id];
    const target = reduced[link.target.id];
    for (const label of link.transition.split('')) {
      if (!states[source].transitions[label]) {
        states[source].transitions[label] = [];
      }
      states[source].transitions[label].push(target);
    }
  }
  return states;
}
