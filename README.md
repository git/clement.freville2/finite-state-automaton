finite-state-automaton
======================

Use
---

### Build-tool

Run your favorite package manager (npm, yarn, pnpm, bun, etc.) to install the dependencies and build the Vite application:

```sh
npm install
npm run dev # Run the Vite development server
npm run build # Build the application in the dist/ directory
```
