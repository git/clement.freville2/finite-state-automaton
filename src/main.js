import { AUTOMATONS } from './examples.js';
import { openAutomaton } from './fsm.js';
import { tr, trUserContent } from './i18n.ts';

const automatonSelector = /** @type {HTMLDivElement} */ (document.getElementById('automaton-selector'));
const automatonCollection = /** @type {HTMLDivElement} */ (document.getElementById('automaton-collection'));
const backButton = /** @type {HTMLButtonElement} */ (document.getElementById('back-button'));
const app = /** @type {HTMLDivElement} */ (document.getElementById('app'));

for (const [displayName, automaton] of Object.entries(AUTOMATONS)) {
  const card = document.createElement('div');
  card.setAttribute('tabindex', '0');
  card.setAttribute('role', 'button');
  card.classList.add('card');
  card.innerHTML = `
        <div class="card-body">
        <h5 class="card-title">${trUserContent(displayName)}</h5>
        <p class="card-text">${automaton.length} ${tr('states')}</p>
        </div>
    `;
  const handleEvent = () => {
    automatonSelector.setAttribute('hidden', 'hidden');
    app.removeAttribute('hidden');
    history.replaceState({ automaton }, '', `#${encodeURIComponent(displayName)}`);
    openAutomaton(trUserContent(displayName), automaton);
  };
  card.addEventListener('click', handleEvent);
  card.addEventListener('keydown', (event) => {
    if (event.key === 'Enter' || event.key === ' ') {
      handleEvent();
    }
  });
  automatonCollection.appendChild(card);
}
const create = document.createElement('div');
create.classList.add('card');
create.innerHTML = `
    <div class="card-body">
    <h5 class="card-title">${tr('Create New Automaton')}</h5>
    <p class="card-text">${tr('Create a new automaton from scratch.')}</p>
    </div>
`;
automatonCollection.appendChild(create);
create.addEventListener('click', () => {
  automatonSelector.setAttribute('hidden', 'hidden');
  app.removeAttribute('hidden');
  openAutomaton('', [], true);
});

const hash = decodeURIComponent(location.hash.slice(1));
if (AUTOMATONS.hasOwnProperty(hash)) {
  openAutomaton(trUserContent(hash), AUTOMATONS[/** @type {keyof typeof AUTOMATONS} */ (hash)]);
  automatonSelector.setAttribute('hidden', 'hidden');
  app.removeAttribute('hidden');
}

backButton.addEventListener('click', () => {
  app.setAttribute('hidden', 'hidden');
  automatonSelector.removeAttribute('hidden');
  history.replaceState({}, '', '#');
});
