const fr = {
  'Select an automaton': 'Sélectionner un automate',
  'Back': 'Retour',
  'Clear': 'Effacer',
  'Delete': 'Supprimer',
  'Controls': 'Contrôles',
  'Action': 'Action',
  'Desktop': 'Bureau',
  'Pan': 'Déplacer',
  'Zoom in/out': 'Zoomer',
  'Create state': 'Créer un état',
  'Move state': 'Déplacer un état',
  'Edit/Delete state': 'Éditer/Supprimer un état',
  'Create transition': 'Créer une transition',
  'Edit transition': 'Éditer une transition',
  'Delete transition': 'Supprimer une transition',
  'Left-click and drag': 'Clic gauche et déplacer',
  'Mouse wheel': 'Molette de la souris',
  'Double-click': 'Double-clic',
  'Middle-click and drag': 'Clic molette et déplacer',
  'Right-click': 'Clic droit',
  'Starts and ends with a': 'Commence et finit par a',
  'Starts with bb': 'Commence par bb',
  'Exactly one b': 'Exactement un b',
  'Odd number of a': 'Nombre impair de a',
  'Ends with two b': 'Finit par deux b',
  'states': 'états',
  'Create New Automaton': 'Créer un nouvel automate',
  'Create a new automaton from scratch.': 'Créer un nouvel automate à partir de zéro.',
  'Start': 'Début',
  'Accepting': 'Acceptant',
  'Display representation': 'Afficher la représentation',
};
type TranslationKey = keyof typeof fr;

let lang = 'en-US';
if (navigator.languages) {
  lang = navigator.languages.find((l) => l !== lang) || lang;
}

const messages: Record<string, string> = lang.startsWith('fr') ? fr : {};
document.querySelectorAll('[data-i18n]').forEach((element) => {
  element.textContent = trUserContent(element.textContent!);
});

export function tr(key: TranslationKey): string {
  return messages[key] || key;
}

export function trUserContent(key: string): string {
  return messages[key] || key;
}
