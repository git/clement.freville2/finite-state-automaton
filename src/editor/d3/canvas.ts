import * as d3 from 'd3';
import { D3ZoomEvent } from 'd3';
import { Graph, Link, Node } from './graph.ts';

export interface GraphConfiguration {
  nodeRadius: number;

  tooltipOpacity: number;
  tooltipFadeInTame: number;
  tooltipFadeOutTime: number;

  markerBoxSize: number;
  markerPadding: number;
  markerRef: number;
  arrowPoints: [number, number][];
  markerPath: string;
}

export type GraphHost = d3.Selection<
  HTMLElement,
  undefined,
  null,
  undefined
>;

export type Canvas = d3.Selection<
  SVGGElement,
  undefined,
  null,
  undefined
>;

export type Simulation = d3.Simulation<Node, Link>;
export type Zoom = d3.ZoomBehavior<SVGSVGElement, undefined>;

export function createCanvas(
  host: GraphHost,
  zoom: Zoom,
  onPointerMoved: (event: PointerEvent) => void,
  onPointerUp: (event: PointerEvent) => void,
  onDoubleClick: (event: PointerEvent) => void,
): Canvas {
  return host
    .append('svg')
    .attr('width', '100%')
    .attr('height', '100%')
    .on('pointermove', (event: PointerEvent) => onPointerMoved(event))
    .on('pointerup', (event: PointerEvent) => onPointerUp(event))
    .on('contextmenu', (event: MouseEvent) => terminate(event))
    .on('dblclick', (event: PointerEvent) => onDoubleClick(event))
    .call(zoom)
    .on('dblclick.zoom', null)
    .append('g');
}

export function createZoom(
  onZoom: (event: D3ZoomEvent<any, any>) => void,
): Zoom {
  return d3
    .zoom<SVGSVGElement, undefined>()
    .scaleExtent([0.1, 10])
    .filter((event) => event.button === 0 || event.touches?.length >= 2)
    .on('zoom', (event) => onZoom(event));
}

export function createSimulation(
  graph: Graph,
  config: GraphConfiguration,
  width: number,
  height: number,
  onTick: () => void,
): Simulation {
  return d3
    .forceSimulation<Node, Link>(graph!.nodes)
    .on('tick', () => onTick())
    .force('center', d3.forceCenter<Node>(width / 2, height / 2))
    .force('charge', d3.forceManyBody<Node>().strength(-500))
    .force('collision', d3.forceCollide<Node>().radius(config.nodeRadius))
    .force(
      'link',
      d3
        .forceLink<Node, Link>()
        .links(graph!.links)
        .id((d: Node) => d.id)
        .distance(config.nodeRadius * 10),
    )
    .force('x', d3.forceX<Node>(width / 2).strength(0.05))
    .force('y', d3.forceY<Node>(height / 2).strength(0.05));
}

function terminate(event: Event): void {
  event.preventDefault();
  event.stopPropagation();
}
